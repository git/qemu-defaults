#!/usr/bin/perl 

use lib '.';
use strict;
use warnings;

use PVE::Cluster qw(cfs_read_file);

use PVE::QemuDefaults;

use Data::Dumper;

#my $cfg = cfs_read_file('qemu-defaults.cfg');

my $cfg = PVE::QemuDefaults::load_defaults();

print Dumper($cfg);
