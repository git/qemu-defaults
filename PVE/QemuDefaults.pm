package PVE::QemuDefaults;

use strict;
use warnings;
use File::chdir;
use File::Path;
use PVE::Tools qw(run_command);
use PVE::JSONSchema qw(get_standard_option);
use PVE::Cluster qw(cfs_register_file);
use PVE::QemuServer;

use Data::Dumper;

use base qw(PVE::SectionConfig);

#cfs_register_file('qemu-defaults.cfg',
#		  sub { __PACKAGE__->parse_config(@_); },
#		  sub { __PACKAGE__->write_config(@_); });

# fixme: remove this, use cfs_register_file above
sub load_defaults {
    #my $filename = "/etc/pve/qemu-defaults.cfg";
    my $filename = "./qemu-defaults.cfg";
    my $raw = PVE::Tools::file_get_contents($filename);
    return __PACKAGE__->parse_config($filename, $raw);
}

my $defaultData = {
    propertyList => {
	type => { 
	    description => "Section type.",
	    type => 'string',
	},
	group => { 
	    description => "User Group.",
	    type => 'string', format => 'pve-groupid',
	},
    },
};

sub private {
    return $defaultData;
}

sub parse_section_header {
    my ($class, $line) = @_;

    if ($line =~ m/^((qm|storage):([^\s\:\@]*)(:([^\s\:\@]+))?)(\s+\@(\S+))?$/) {
	my ($sid, $type, $pool, $subid, $group) = ($1, $2, $3, $5, $7);
	my $errmsg = undef; # set if you want to skip whole section
	my $config = {}; # to return additional attributes
	$sid = "$sid\@$group" if defined($group);
	eval {
	    if ($type eq 'qm') {
		if ($subid) {
		    __PACKAGE__->check_value($type, 'ostype', $subid, $sid);
		    $config->{ostype} = $subid;
		}

	    } elsif ($type eq 'storage') {
		if ($subid) {
		    PVE::JSONSchema::parse_storage_id($subid);
		    $config->{storage} = $subid;
		}
	    } 
	    if ($group) {
		__PACKAGE__->check_value($type, 'group', $group, $sid);
		$config->{group} = $group;
	    }
	};
	$errmsg = $@ if $@;
	$config->{pool} = $pool if $pool;
	return ($type, $sid, $errmsg, $config);
    }
    return undef;
}

sub parse_config {
    my ($class, $filename, $raw) = @_;

    my $cfg = $class->SUPER::parse_config($filename, $raw);

    return $cfg;
}

package PVE::QemuDefaults::qm;

use PVE::JSONSchema qw(get_standard_option);
use base qw(PVE::QemuDefaults);

my $option_list = ['ostype', 'memory', 'sockets', 'cores'];

sub type {
    return 'qm';
}

sub properties {
    my $props = {
	pool => {
	    description => "VM pool.",
	    type => 'string', format => 'pve-poolid',
	},
    };

    foreach my $k (@$option_list) {
	$props->{$k} = get_standard_option("pve-qm-$k");
    }

    return $props;
}

sub options {
    my $opts = {
	group => { optional => 1 },
 	pool => { optional => 1 },
    };

    foreach my $k (@$option_list) {
	$opts->{$k} = { optional => 1 };
    }

    return $opts;
}

__PACKAGE__->register();

package PVE::QemuDefaults::storage;

use PVE::JSONSchema qw(get_standard_option);
use base qw(PVE::QemuDefaults);

sub type {
    return 'storage';
}

sub properties {
    return {
	storage => get_standard_option('pve-storage-id'),
    };
}

sub options {
     return {
	group => { optional => 1 },
	pool => { optional => 1 },
	storage => { optional => 1 },
   };
}

__PACKAGE__->register();


PVE::QemuDefaults->init();

1;
